package com.cgi.usermanagement.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Table(name = "\"address\"")
@SequenceGenerator(
        name = Address.SEQUENCE_GENERATOR_NAME,
        sequenceName = "address_id_address_seq"
)
@Builder
@Entity
public class Address implements Serializable {

    static final String SEQUENCE_GENERATOR_NAME = "address_generator";

    @Id
    @GeneratedValue(generator = SEQUENCE_GENERATOR_NAME)
    @Column(name = "id_address")
    private Long addressId;
    private String city;
    private String country;
    private String street;
    @Column(name = "post_code")
    private String postCode;

}
