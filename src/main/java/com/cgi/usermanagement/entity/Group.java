package com.cgi.usermanagement.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "\"group\"")
@SequenceGenerator(
        name = Group.SEQUENCE_GENERATOR_NAME,
        sequenceName = "group_id_group_seq"
)
@Builder
@Entity
public class Group implements Serializable {

    static final String SEQUENCE_GENERATOR_NAME = "group_generator";

    @Id
    @Column(name = "id_group")
    @GeneratedValue(generator = SEQUENCE_GENERATOR_NAME)
    private Long groupId;

    private String name;

    private String description;

    @Column(name = "exp_date")
    private LocalDate expDate;

    @ManyToMany(fetch = FetchType.LAZY, targetEntity = Person.class, mappedBy = "groups")
    private List<Person> persons;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "GROUP_ROLE", joinColumns = {@JoinColumn(name = "id_group")}, inverseJoinColumns = {
            @JoinColumn(name = "id_role")})
    private List<Role> roles;
}
