package com.cgi.usermanagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.springframework.data.jpa.repository.EntityGraph;

import javax.inject.Named;
import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "\"person\"")
@SequenceGenerator(
        name = Group.SEQUENCE_GENERATOR_NAME,
        sequenceName = "person_id_person_seq"
)
@Entity
public class Person implements Serializable {

    static final String SEQUENCE_GENERATOR_NAME = "person_generator";


    @Column(name = "id_person")
    @GeneratedValue(generator = SEQUENCE_GENERATOR_NAME)
    @Id
    private Long personId;
    private String name;
    private String email;
    private String password;
    private Double salary;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_address")
    private Address address;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "PERSON_GROUP", joinColumns = {@JoinColumn(name = "id_person")}, inverseJoinColumns = {
            @JoinColumn(name = "id_group")})
    private List<Group> groups;
}
