package com.cgi.usermanagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "role")
@SequenceGenerator(
        name = Group.SEQUENCE_GENERATOR_NAME,
        sequenceName = "role_id_role_seq"
)
@Builder
@Entity
public class Role implements Serializable {

    static final String SEQUENCE_GENERATOR_NAME = "role_generator";

    @Column(name = "id_role")
    @GeneratedValue(generator = SEQUENCE_GENERATOR_NAME)
    @Id
    private Long roleId;

    @Column(name = "role_type")
    private String roleType;

    private String description;

    @ManyToMany(fetch = FetchType.LAZY, targetEntity = Group.class, mappedBy = "roles")
    private List<Group> groups;
}
