package com.cgi.usermanagement;

import com.cgi.usermanagement.entity.Person;
import com.cgi.usermanagement.messaging.producer.JmsPersonMessagingProducerService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class UsermanagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsermanagementApplication.class, args);
    }

}
