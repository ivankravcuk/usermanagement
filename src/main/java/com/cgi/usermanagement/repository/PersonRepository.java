package com.cgi.usermanagement.repository;

import com.cgi.usermanagement.entity.Person;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long>, QuerydslPredicateExecutor<Person> {
    @Override
    @EntityGraph(attributePaths = {"address", "groups"})
    Page<Person> findAll(Predicate predicate, Pageable pageable);

    @Query("SELECT p FROM Person p JOIN FETCH p.address a WHERE p.personId = :personId")
    Optional<Person> findPersonById(@Param("personId") Long personId);
}
