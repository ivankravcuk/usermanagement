package com.cgi.usermanagement.rest.exceptionhandling;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiError {
    final private LocalDateTime timestamp = LocalDateTime.now(Clock.systemUTC().withZone(ZoneId.of("Europe/Prague")));
    private HttpStatus status;
    private String message;
    @Singular
    private List<String> errors;
    private String path;

}
