package com.cgi.usermanagement.rest.controller;


import com.cgi.usermanagement.entity.Person;
import com.cgi.usermanagement.rest.exceptions.ResourceNotFoundException;
import com.cgi.usermanagement.rest.model.PersonDto;
import com.cgi.usermanagement.rest.model.PersonDtoWithSalaryAndGroups;
import com.cgi.usermanagement.rest.model.RoleDto;
import com.cgi.usermanagement.service.PersonService;
import com.querydsl.core.types.Predicate;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/persons")
public class PersonController {


    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAllPersons(
            @QuerydslPredicate(root = Person.class) Predicate predicate,
            Pageable pageable,
            @RequestParam MultiValueMap<String, String> parameters,
            @RequestParam(value = "fields", required = false) String fields) {

        try {
            return ResponseEntity.ok(personService.findAllPersons(predicate, pageable));
        } catch (ServiceException e) {
            throw new ResourceNotFoundException(e);
        }
    }

    @ApiOperation(
            value = "Get person by Id",
            produces = "json",
            httpMethod = "GET",
            response = PersonDto.class,
            nickname = "findTicketById"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "The requested resource was not found")
    })
    @GetMapping(path = "/{id}")
    public ResponseEntity<PersonDto> findPersonById(
            @ApiParam(value = "ID of the person")
            @PathVariable("id") Long id) {

        try {
            return ResponseEntity.ok(personService.findPersonById(id));
        } catch (ServiceException e) {
            throw new ResourceNotFoundException(e);
        }
    }

    @GetMapping(path = "/details",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAllPersonsDetails(
            @QuerydslPredicate(root = Person.class) Predicate predicate,
            Pageable pageable,
            @RequestParam MultiValueMap<String, String> parameters,
            @RequestParam(value = "fields", required = false) String fields) {

        try {
            return ResponseEntity.ok(personService.findAllPersonsDetail(predicate, pageable));
        } catch (ServiceException e) {
            throw new ResourceNotFoundException(e);
        }
    }

    @GetMapping(path = "/{id}/details")
    public ResponseEntity<PersonDtoWithSalaryAndGroups> findPersonByIdDetails(
            @ApiParam(value = "ID of the person")
            @PathVariable("id") Long id) {

        try {
            return ResponseEntity.ok(personService.findPersonByIdDetail(id));
        } catch (ServiceException e) {
            throw new ResourceNotFoundException(e);
        }
    }

    @GetMapping(path = "/{id}/roles")
    public ResponseEntity<List<RoleDto>> findPersonGroupsById(
            @ApiParam(value = "ID of the person")
            @PathVariable("id") Long id) {

        try {
            return ResponseEntity.ok(personService.findPersonRolesById(id));
        } catch (ServiceException e) {
            throw new ResourceNotFoundException(e);
        }
    }
}
