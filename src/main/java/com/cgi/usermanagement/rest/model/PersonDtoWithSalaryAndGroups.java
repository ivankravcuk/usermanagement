package com.cgi.usermanagement.rest.model;

import com.cgi.usermanagement.entity.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonDtoWithSalaryAndGroups {

    private Long personID;

    private String name;

    private String email;

    private Address address;

    private Double salary;

    private List<GroupDto> groups;
}
