package com.cgi.usermanagement.rest.model;

import com.cgi.usermanagement.entity.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonDto {

    private Long personID;

    private String name;

    private String email;

    private Address address;
}
