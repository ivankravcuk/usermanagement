package com.cgi.usermanagement.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonGroupDto {
    @JsonIgnore
    private Set<GroupDto> groups;
}
