package com.cgi.usermanagement.rest.model;

import com.cgi.usermanagement.entity.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GroupDto {

    private Long groupId;

    private String name;

    private String description;

    private LocalDate expDate;

    private Set<RoleDto> roles;

}
