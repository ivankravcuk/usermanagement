package com.cgi.usermanagement.rest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonCreateDto {
    @Email
    private String email;
    @Pattern(regexp = ".*")
    @Size(min = 6, max = 45)
    private String password;
    @NotEmpty(message = "It is required to provide firstName.")
    private String firstName;
    @NotEmpty(message = "It is required to provide nickname.")
    private String nickname;
    @NotEmpty(message = "It is required to provide surname.")
    private String surname;
}
