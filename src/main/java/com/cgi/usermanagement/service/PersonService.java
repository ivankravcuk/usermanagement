package com.cgi.usermanagement.service;

import com.cgi.usermanagement.rest.model.PersonDto;
import com.cgi.usermanagement.rest.model.PersonDtoWithSalaryAndGroups;
import com.cgi.usermanagement.rest.model.RoleDto;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PersonService {

    //Peasant requests
    Page<PersonDto> findAllPersons(Predicate predicate, Pageable pageable);

    PersonDto findPersonById(Long id);

    //Manager requests
    PersonDtoWithSalaryAndGroups findPersonByIdDetail(Long id);

    Page<PersonDtoWithSalaryAndGroups> findAllPersonsDetail(Predicate predicate, Pageable pageable);

    List<RoleDto> findPersonRolesById(Long id);
}
