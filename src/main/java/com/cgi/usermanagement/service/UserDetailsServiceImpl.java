package com.cgi.usermanagement.service;

import com.cgi.usermanagement.entity.Person;
import com.cgi.usermanagement.repository.PersonRepository;
import com.cgi.usermanagement.rest.model.PersonDtoWithSalaryAndGroups;
import com.cgi.usermanagement.security.MyUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;


@Service("proprietaryUserDetailsService")
public class UserDetailsServiceImpl
//        implements UserDetailsService
{

    private PersonRepository personRepository;

    @Autowired
    public UserDetailsServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }
//
//    @Override
//    public UserDetails loadUserByUsername(String id) throws EntityNotFoundException, UsernameNotFoundException {
//        // use different method to retrieve Person also with roles (not with addresses...)
//        Person person = personRepository.findPersonById(Long.parseLong(id))
//                .orElseThrow(() -> new EntityNotFoundException("User not found."));
//        System.out.println(person);
//        return new MyUserPrincipal(person);
//    }
}
