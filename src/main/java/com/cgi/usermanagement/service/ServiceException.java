package com.cgi.usermanagement.service;

public class ServiceException extends RuntimeException {

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable ex) {
        super(message, ex);
    }

    public ServiceException(Throwable ex) {
        super(ex);
    }

}
