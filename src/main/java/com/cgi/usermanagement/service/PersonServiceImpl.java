package com.cgi.usermanagement.service;

import com.cgi.usermanagement.repository.PersonRepository;
import com.cgi.usermanagement.rest.model.PersonDto;
import com.cgi.usermanagement.rest.model.PersonDtoWithSalaryAndGroups;
import com.cgi.usermanagement.rest.model.RoleDto;
import com.cgi.usermanagement.service.mapping.BeanMapping;
import com.querydsl.core.types.Predicate;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    PersonRepository personRepository;
    BeanMapping beanMapping;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository, BeanMapping beanMapping) {
        this.personRepository = personRepository;
        this.beanMapping = beanMapping;
    }

    @Override
    public Page<PersonDto> findAllPersons(Predicate predicate, Pageable pageable) {
        try {
            return beanMapping.mapTo(personRepository.findAll(predicate, pageable), PersonDto.class);
        } catch (HibernateException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public PersonDto findPersonById(Long id) {
        try {
            return beanMapping.mapTo(personRepository.findPersonById(id), PersonDto.class);
        } catch (NullPointerException e) {
            throw new ServiceException("Person ID is null");
        } catch (HibernateException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public PersonDtoWithSalaryAndGroups findPersonByIdDetail(Long id) {
        try {
            return beanMapping.mapTo(personRepository.findPersonById(id), PersonDtoWithSalaryAndGroups.class);
        } catch (NullPointerException e) {
            throw new ServiceException("Person ID is null");
        } catch (HibernateException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<PersonDtoWithSalaryAndGroups> findAllPersonsDetail(Predicate predicate, Pageable pageable) {
        try {
            return beanMapping.mapTo(personRepository.findAll(predicate, pageable), PersonDtoWithSalaryAndGroups.class);
        } catch (HibernateException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<RoleDto> findPersonRolesById(Long id) {

        Set<RoleDto> roles = new HashSet<>();
        findPersonByIdDetail(id).getGroups().forEach(
                g -> roles.addAll(g.getRoles()));

        try {
            return new ArrayList<>(roles);
        } catch (NullPointerException e) {
            throw new ServiceException("Person ID is null");
        } catch (HibernateException e) {
            throw new ServiceException(e);
        }
    }

}